package cn.outshine.stats.dao;

import java.util.List;

import cn.outshine.stats.model.User;
import cn.outshine.stats.util.HibernateUtil;


import org.hibernate.Query;
import org.hibernate.Session;

public class UserDAOHibernate implements UserDAO
{
	
    public UserDAOHibernate()
    {
    }

    public User get(Integer id) throws Exception
    {
    	Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Query query = s.createQuery("from User entity where entity.id= :id");
        query.setString("id", id.toString());
        User user = query.list().size() <= 0 ? null : (User) query.list().get(0);
        return user;
    }

    public List getList() throws Exception
    {
    	Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Query query = s.createQuery("from User entity");
        List list = query.list().size() <= 0 ? null :  query.list();
        return list;
    }
    
    public User get(String username) throws Exception
    {
    	Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Query query = s.createQuery("from User entity where entity.username= :username");
        query.setString("username", username);
        User user = query.list().size() <= 0 ? null : (User) query.list().get(0);
        return user;
    }
    
    public void save(User o) throws Exception
    {
    	Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        s.merge(o);
    }

    public User get(String username, String password) throws Exception
    {
    	
    	Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Query query = s.createQuery("from User entity where entity.username= :username and entity.password= :password");
        query.setString("username", username);
        query.setString("password", password);
        User user = query.list().size() <= 0 ? null : (User) query.list().get(0);
        return user;
    }

}
