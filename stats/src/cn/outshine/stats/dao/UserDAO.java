package cn.outshine.stats.dao;

import java.util.List;

import cn.outshine.stats.model.User;

public interface UserDAO
{

    public abstract User get(Integer integer) throws Exception;
    
    public abstract List getList() throws Exception;
    
    public abstract User get(String username) throws Exception;

    public abstract void save(User user) throws Exception;

    public abstract User get(String s, String s1) throws Exception;
    
}
