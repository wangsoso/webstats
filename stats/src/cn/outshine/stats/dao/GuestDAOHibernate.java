package cn.outshine.stats.dao;

import cn.outshine.stats.bean.Paginator;
import cn.outshine.stats.model.Guest;
import cn.outshine.stats.util.DateTimeUtil;
import cn.outshine.stats.util.HibernateUtil;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Query;

public class GuestDAOHibernate extends GuestDAO
{

    public GuestDAOHibernate()
    {
    }
    
    public void save(Guest o) throws Exception
    {
    	Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.save(o);
		s.flush();
    }

    public Paginator getPaginatorByWebsiteId(Integer userId, String ip, Integer range, Integer p) throws HibernateException
    {
    	Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Paginator paginator = new Paginator();
        paginator.setRange(range.intValue());
        paginator.setP(p.intValue());
        StringBuffer sb = new StringBuffer();
        StringBuffer sb1 = new StringBuffer();
        sb.append("from Guest as e where 1=1");
        if(userId != null && !userId.equals(""))
        {
            sb1.append(" and e.user.id = ");
            sb1.append(userId);
        }
        if(ip != null && !ip.equals(""))
        {
            sb1.append(" and e.ip like ");
            sb1.append("'%" + ip + "%'");
        }
        Object itemCount = s.createQuery("select count(*) from Guest as e where 1=1"+sb1.toString()).uniqueResult();
        Long count = (itemCount == null)? 0 : (Long)itemCount;
        paginator.setCount(count.intValue());
        int pageCount = 1;
        if(count.intValue() > range.intValue())
            pageCount = (int)Math.ceil((double)count.intValue() / (double)range.intValue());
        paginator.setC(pageCount);
        sb1.append(" order by e.time desc");
        Query query = s.createQuery(sb.toString()+sb1.toString());
        query.setMaxResults(range.intValue());
        query.setFirstResult((p.intValue() - 1) * range.intValue());
        paginator.setList(query.list());
        return paginator;
    }
    
    
    public Guest validateIP(String website, String ip) throws Exception
    {
    	Session s = HibernateUtil.getSessionFactory().getCurrentSession();  	
        Query query = s.createQuery("from Guest entity where entity.ip = :ip and entity.website = :website and entity.time > :beginTime and entity.time < :endTime");
        query.setString("ip", ip);
        query.setString("website", website);
        query.setString("beginTime", DateTimeUtil.getbeginTime(-60));
        query.setString("endTime", DateTimeUtil.getEndTime());
        Guest guest = query.list().size() <= 0 ? null : (Guest) query.list().get(0);
        return guest;
    }
	
}
