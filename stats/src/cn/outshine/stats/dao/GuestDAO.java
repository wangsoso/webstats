package cn.outshine.stats.dao;

import org.hibernate.HibernateException;

import cn.outshine.stats.bean.Paginator;
import cn.outshine.stats.model.Guest;


public abstract class GuestDAO
{
	private static GuestDAO guest = null;
	private static Object initLock = new Object();
	private static String className = "cn.outshine.stats.dao.GuestDAOHibernate";
    
    public static GuestDAO getInstance() {
        if (guest == null) {
            synchronized(initLock) {
                if (guest == null) {
                    try {
                        //Load the class and create an instance.
                        Class c = Class.forName(className);
                        guest = (GuestDAO)c.newInstance();
                    }
                    catch (Exception e) {
                        System.err.println("Failed to load GuestFactory class "
                            + className);
                        e.printStackTrace();
                        return null;
                    }
                }
            }
        }
        return guest;
    }
    
    public abstract void save(Guest entry) throws Exception;

    public abstract Paginator getPaginatorByWebsiteId(Integer userId, String ip, Integer range, Integer p) throws HibernateException;

    public abstract Guest validateIP(String website, String ip) throws Exception;
}
