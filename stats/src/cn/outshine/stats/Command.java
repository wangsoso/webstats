package cn.outshine.stats;

import javax.servlet.http.HttpServletResponse;

import cn.outshine.stats.ExecutionContext;

import freemarker.template.SimpleHash;
import freemarker.template.Template;

public abstract class Command
{
	private static Class[] NO_ARGS_CLASS = new Class[0];
	private static Object[] NO_ARGS_OBJECT = new Object[0];
		
	protected String templateName;
	protected static RequestContext request;
	protected HttpServletResponse response;
	protected SimpleHash context;
    
	protected void setTemplateName(String templateName)
	{
		this.templateName = templateName;
	}

	public Template process(RequestContext request, 
			HttpServletResponse response, SimpleHash context) throws Exception 
	{
		this.request = request;
		this.response = response;
		this.context = context;
	
		try {
			this.getClass().getMethod(request.getAction(), NO_ARGS_CLASS).invoke(this, NO_ARGS_OBJECT);
		}
		catch (NoSuchMethodException e) {	
			System.out.println(e);
			this.setTemplateName("empty.htm");
			response.sendError(HttpServletResponse.SC_NOT_FOUND);	
		}
		catch (Exception e) {
			throw e;
		}	
		
		if (ExecutionContext.isCustomContent()) {
			return null;
		}
		
		if (this.templateName == null) {
			this.setTemplateName("empty.htm");
		}
		
		return ExecutionContext.templateConfig().getTemplate(this.templateName.toString());
	}
}
