package cn.outshine.stats;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;

import javax.servlet.*;
import javax.servlet.http.*;

import org.hibernate.HibernateException;

import cn.outshine.stats.ExecutionContext;
import cn.outshine.stats.Global;
import cn.outshine.stats.RequestContext;
import cn.outshine.stats.model.User;
import cn.outshine.stats.util.HibernateUtil;


import freemarker.template.Configuration;
import freemarker.template.SimpleHash;
import freemarker.template.Template;

public class ActionServlet extends HttpServlet 
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
        try {
        	String appPath = config.getServletContext().getRealPath("");
        	new Global(appPath);
            Configuration templateCfg = new Configuration();
            templateCfg.setDirectoryForTemplateLoading(new File(appPath));
            templateCfg.setTemplateUpdateDelay(30);
            templateCfg.setDefaultEncoding(Global.ENCODING); 
			templateCfg.setSharedVariable("startupTime", new Long(new Date().getTime()));
            ExecutionContext.setTemplateConfig(templateCfg);
        } catch (Exception e) {
            throw new ServletException("Error while starting website", e);
        }
	}
	
	public void service(HttpServletRequest req, HttpServletResponse response) throws IOException, ServletException
	{
		Writer out = null;
		try {
			HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
			String encoding = Global.ENCODING;
			RequestContext request = new RequestContext(req);
			ExecutionContext ex = ExecutionContext.get();
			ex.setResponse(response);
			ex.setRequest(request);
			ExecutionContext.set(ex);
			
			SimpleHash context = ExecutionContext.getTemplateContext();   
			context.put("contextPath", request.getContextPath());
			context.put("encoding", encoding);
			
	    	HttpSession session = request.getSession();
	    	User user = (User)session.getAttribute(Global.LOGIN_KEY);
			
			if (user!= null) {
				context.put("user", user);
			}
			
			String module = request.getServletPath().replaceAll("/", "").replaceAll(".do", "");
			String moduleClass = module != null ? Global.getModuleClass(module) : null;
			
			context.put("moduleName", module);
			context.put("action", request.getAction());

			if (moduleClass != null) {
				Command c = (Command)Class.forName(moduleClass).newInstance();
				Template template = c.process(request, response, context);
				if (ExecutionContext.getRedirectTo() == null) {
					String contentType = ExecutionContext.getContentType();
					
					if (contentType == null) {
						contentType = "text/html; charset=" + encoding;
					}
					
					response.setContentType(contentType);
					if (!ExecutionContext.isCustomContent()) {
						out = new BufferedWriter(new OutputStreamWriter(response.getOutputStream(), encoding));
						template.process(ExecutionContext.getTemplateContext(), out);
						out.flush();
					}
				}
			}
			else {
				// Module not found, send 404 not found response
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}
			
		}
		catch (Exception e) {
			System.out.println(e);
			return;
		}	
		finally {
            try{
            	HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
            }catch (HibernateException e){
            	HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().rollback();
            	throw new HibernateException(e);
            }
			try {
				if (out != null) { out.close(); }
			}
			catch (Exception e) {}
			String redirectTo = ExecutionContext.getRedirectTo();
			ExecutionContext.finish();
			if (redirectTo != null) {
				response.sendRedirect(redirectTo);
			}
		}
	}

	public void destroy() {
		super.destroy();
		System.out.println("destroying website...");
	}
	
}
