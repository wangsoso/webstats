package cn.outshine.stats.util; 
  
import java.io.UnsupportedEncodingException; 
import java.net.URLDecoder; 
import java.util.regex.Matcher; 
import java.util.regex.Pattern; 
  
public class ParseURLKeyword { 
    public static void main(String[] args) { 
    	String url = "http://cn.bing.com/search?q=17%E5%B2%81%E5%A9%90%E4%BD%93%E7%BE%8E%E5%B0%91%E5%A5%B3%E8%82%9B&form=CICMK1"; 
        System.out.println(ParseURLKeyword.getKeyword(url)); 
        System.out.println(""); 
    } 
     
  public static String getKeyword(String url){ 
        String keywordReg = "(?:yahoo.+?[\\?|&]p=|google.+?q=|sogou.+?query=|so\\.360\\.cn.+?q=|bing\\.com.+?q=|cn\\.bing\\.com.+?q=|baidu\\.com.+?url=|baidu.+?wd=|baidu.+?word=|m\\.baidu\\.com.+?word=|soso.+?w=|so.+?q=|3721\\.com.+?p=)([^&]*)"; 
        String encodeReg = "^(?:[\\x00-\\x7f]|[\\xfc-\\xff][\\x80-\\xbf]{5}|[\\xf8-\\xfb][\\x80-\\xbf]{4}|[\\xf0-\\xf7][\\x80-\\xbf]{3}|[\\xe0-\\xef][\\x80-\\xbf]{2}|[\\xc0-\\xdf][\\x80-\\xbf])+$"; 
         
        Pattern keywordPatt = Pattern.compile(keywordReg); 
        StringBuffer keyword = new StringBuffer(20); 
        Matcher keywordMat = keywordPatt.matcher(url); 
        while (keywordMat.find()) { 
            keywordMat.appendReplacement(keyword, "$1"); 
        } 
        if (!keyword.toString().equals("")){ 
            String keywordsTmp = keyword.toString().replace("http://www.", "").replace("https://www.", "");
            keywordsTmp = keywordsTmp.toString().replace("http://", "").replace("https://", ""); 
            Pattern encodePatt = Pattern.compile(encodeReg); 
            String unescapeString = ParseURLKeyword.unescape(keywordsTmp); 
            Matcher encodeMat = encodePatt.matcher(unescapeString); 
            String encodeString = "gbk"; 
            if (encodeMat.matches()) encodeString = "utf-8"; 
            try {
                return URLDecoder.decode(keywordsTmp, encodeString); 
            } catch (UnsupportedEncodingException e) { 
                return ""; 
            } 
        } 
        return ""; 
    } 
     
    public static String unescape(String src) { 
        StringBuffer tmp = new StringBuffer(); 
        tmp.ensureCapacity(src.length()); 
        int lastPos = 0, pos = 0; 
        char ch; 
        while (lastPos < src.length()) { 
            pos = src.indexOf("%", lastPos); 
            if (pos == lastPos) { 
                if (src.charAt(pos + 1) == 'u') { 
                    ch = (char) Integer.parseInt(src.substring(pos + 2, pos + 6), 16); 
                    tmp.append(ch); 
                    lastPos = pos + 6; 
                } else { 
                    ch = (char) Integer.parseInt(src.substring(pos + 1, pos + 3), 16); 
                    tmp.append(ch); 
                    lastPos = pos + 3; 
                } 
            } else { 
                if (pos == -1) { 
                    tmp.append(src.substring(lastPos)); 
                    lastPos = src.length(); 
                } else { 
                    tmp.append(src.substring(lastPos, pos)); 
                    lastPos = pos; 
                } 
            } 
        } 
        return tmp.toString(); 
    } 
}