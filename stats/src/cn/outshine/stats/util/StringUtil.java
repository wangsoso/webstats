package cn.outshine.stats.util;

import org.apache.commons.lang.StringUtils;

public class StringUtil
{

	public final static int MAX_COUNT = 30;
	public final static int MAX_COUNT2 = 1000;
	
    public StringUtil()
    {
    }

    public static boolean empty(String param)
    {
        return param == null || param.trim().length() < 1;
    }

    public static String nvl(String param)
    {
        return param != null ? param.trim() : "";
    }

    public static int parseInt(String param, int d)
    {
        int i = d;
        try
        {
            if(!empty(param))
                i = Integer.parseInt(param);
        }
        catch(Exception exception) { }
        return i;
    }

    public static int parseInt(String param)
    {
        return parseInt(param, 0);
    }

    public static long parseLong(String param)
    {
        long l = 0L;
        try
        {
            if(!empty(param))
                l = Long.parseLong(param);
        }
        catch(Exception exception) { }
        return l;
    }

    public static boolean parseBoolean(String param)
    {
        if(empty(param))
            return false;
        switch(param.charAt(0))
        {
        case 49: // '1'
        case 84: // 'T'
        case 89: // 'Y'
        case 116: // 't'
        case 121: // 'y'
            return true;
        }
        return false;
    }

    public static String escapeXml(String str)
    {
        str = StringUtils.replace(str, "&", "&amp;");
        str = StringUtils.replace(str, "<", "&lt;");
        str = StringUtils.replace(str, ">", "&gt;");
        str = StringUtils.replace(str, "\"", "&quot;");
        str = StringUtils.replace(str, "'", "&apos;");
        return str;
    }

    public static String removeXml(String str)
    {
        int sz = str.length();
        StringBuffer buffer = new StringBuffer(sz);
        boolean inTag = false;
        for(int i = 0; i < sz; i++)
        {
            char ch = str.charAt(i);
            if(ch == '<')
                inTag = true;
            else
            if(ch == '>')
            {
                inTag = false;
                continue;
            }
            if(!inTag)
                buffer.append(ch);
        }

        return buffer.toString();
    }

    public static boolean isAsciiOrDigit(String name){
        for(int i=0;i<name.length();i++){
            char ch = name.charAt(i);
            if(!isAscii(ch))
            	return false;
        }
        return true;
    }
    
    public static boolean isAscii(char ch){
    	return (ch >='a' && ch <='z') || (ch >='A' && ch <='Z') || (ch >='0' && ch <='9') || (ch =='_');
    }
    
    public static String truncateNicely(String str, int lower, int upper, String appendToEnd)
    {
        str = removeXml(str);
        if(upper < lower)
            upper = lower;
        if(str.length() > upper)
        {
            int loc = str.lastIndexOf(' ', upper);
            if(loc >= lower)
                str = str.substring(0, loc);
            else
                str = str.substring(0, upper);
            str = str + appendToEnd;
        }
        return str;
    }

}
