package cn.outshine.stats.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class DateTimeUtil
{

    public DateTimeUtil()
    {
    }

    public static String getbeginTime(int minute){
    	Calendar calendar = new GregorianCalendar();
    	calendar.add(Calendar.MINUTE,minute);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(calendar.getTime());
        return dateString;
    }
    
    public static String getEndTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String endTime = formatter.format(new Date());
        return endTime;
    }
    
    public static String now(String pattern)
    {
        return dateToString(new Date(), pattern);
    }

    public static String dateToString(Date date, String pattern, Locale locale)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, locale);
        return sdf.format(date);
    }

    public static String dateToString(Date date, String pattern)
    {
        Locale locale = Locale.getDefault();
        return dateToString(date, pattern, locale);
    }

    public static long stringToLong(String strDate, String pattern, Locale locale)
        throws ParseException
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, locale);
        Date date = simpleDateFormat.parse(strDate);
        return date.getTime();
    }

    public static long stringToLong(String strDate, String pattern)
        throws ParseException
    {
        Locale locale = Locale.getDefault();
        return stringToLong(strDate, pattern, locale);
    }
}
