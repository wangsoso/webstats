package cn.outshine.stats.action;

import javax.servlet.http.HttpSession;

import cn.outshine.stats.Global;
import cn.outshine.stats.dao.UserDAO;
import cn.outshine.stats.dao.UserDAOHibernate;
import cn.outshine.stats.model.User;


public class UserAction extends AdminCommand
{

	private UserDAO userDAO = new UserDAOHibernate();
	
    public UserAction()
    {
    }
    
    public void edit() throws Exception
    {
        HttpSession sess = request.getSession();
        User user = (User)sess.getAttribute(Global.LOGIN_KEY);
        sess.setAttribute(Global.LOGIN_KEY, user);
        if(user == null)
        {
        	this.context.put(Global.LOGIN_KEY, null);
        	this.context.put("moduleName", "user");
        	this.context.put("action", "validateLogin");
        	this.context.put("moduleAction", "login.htm");
        } else
        {
            this.context.put("user", user);
        	this.context.put("moduleName", "user");
        	this.context.put("action", "save");
        	this.context.put("moduleAction", "user.htm");
        }
    }
    
    public void save() throws Exception
    {
    	
    	String password = request.getParameter("password");
    	String confirmPassword = request.getParameter("confirmPassword");
        if(!password.equals(confirmPassword))
        {
        	this.context.put("info", "�������!");
        } else
        {
        	HttpSession session = request.getSession();
            User user = userDAO.get(((User)session.getAttribute(Global.LOGIN_KEY)).getId());
            user.setPassword(password);
            userDAO.save(user);
            this.context.put("info", "���뱣��ɹ�!");
        }
        HttpSession sess = request.getSession();
        this.context.put("user", sess.getAttribute(Global.LOGIN_KEY));

    	this.context.put("moduleName", "user");
    	this.context.put("action", "save");
    	this.context.put("moduleAction", "user.htm");
    }

	public void list() throws Exception {
		// TODO Auto-generated method stub
	}
}
