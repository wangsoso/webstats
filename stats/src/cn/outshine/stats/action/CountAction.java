package cn.outshine.stats.action;

import java.util.Date;

import cn.outshine.stats.Command;
import cn.outshine.stats.ExecutionContext;
import cn.outshine.stats.dao.*;
import cn.outshine.stats.model.Guest;
import cn.outshine.stats.model.User;
import cn.outshine.stats.util.IPSeeker;
import cn.outshine.stats.util.ParseURLKeyword;
import cn.outshine.stats.util.StringUtil;



public class CountAction extends Command
{
	
    public CountAction()
    {
    }

    public void save() throws Exception
    {
		UserDAO userDAO = new UserDAOHibernate();
		Guest guest = new Guest();
		System.out.println("");
    	String username = request.getParameter("username");
    	String webpage = request.getParameter("webpage");
    	String referer = request.getParameter("referer");
		String userAgent = request.getHeader("User-Agent");
		userAgent = userAgent.toLowerCase();
		String alexa = "��";
		if (userAgent.indexOf("alexa") != -1) {
			alexa = "��";
		}
		
    	if(referer == null) referer="";
    	if(referer.length()>255) referer=referer.substring(0,254);
        if(StringUtil.empty(username)) username="invalid";
    	if(StringUtil.empty(webpage)) webpage="index";
    	String keyword="";
    	if(referer.length()>5){
    		keyword = ParseURLKeyword.getKeyword(referer);
        	if(keyword.length()>50) keyword=keyword.substring(0,49);
    	}
        User user = null;
		try {
			user = userDAO.get(username);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		if(user != null){
	    	String ip = request.getRemoteAddr();
	    	guest.setIp(ip);
	        Date date = new Date();
			guest.setWebsite(user.getWebsite());
	        guest.setWebpage(webpage);
	        guest.setKeyword(keyword);
	        guest.setAlexa(alexa);
	        guest.setReferer(referer);
	        guest.setTime(date);
	        guest.setUser(user);
	    	try {
	    		GuestDAO guestDAO = GuestDAO.getInstance();
				if(guestDAO.validateIP(user.getWebsite(), ip) == null){
					IPSeeker ipSeeker = new IPSeeker();
					String address = ipSeeker.getAddress(ip);
			        guest.setArea(address);
					guestDAO.save(guest);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
    	ExecutionContext.enableCustomContent(true);
    }
}
