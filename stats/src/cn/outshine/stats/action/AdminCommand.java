package cn.outshine.stats.action;

import cn.outshine.stats.Command;
import cn.outshine.stats.ExecutionContext;
import cn.outshine.stats.Global;
import cn.outshine.stats.RequestContext;
import cn.outshine.stats.model.User;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import freemarker.template.SimpleHash;
import freemarker.template.Template;

public abstract class AdminCommand extends Command
{
	public Template process(RequestContext request, HttpServletResponse response,
			SimpleHash context) throws Exception
	{
    	HttpSession session = request.getSession();
    	User user = (User)session.getAttribute(Global.LOGIN_KEY);
		if(user==null){
			ExecutionContext.setRedirect(request.getContextPath()+"/login.do?action=login");
			return null;
		}
		
		if (this.templateName == null) {
			this.templateName =  "admin/_layout.htm";
		}
		
		return super.process(request, response, context);
	}
}
