package cn.outshine.stats.action;

import javax.servlet.http.HttpSession;

import cn.outshine.stats.Command;
import cn.outshine.stats.Global;
import cn.outshine.stats.dao.UserDAO;
import cn.outshine.stats.dao.UserDAOHibernate;
import cn.outshine.stats.model.User;


public class LoginAction extends Command
{
	private UserDAO userDAO = new UserDAOHibernate();

    public LoginAction()
    {
    }
    
    public void logout() throws Exception
    {
        HttpSession session = request.getSession(false);
        session.removeAttribute(Global.LOGIN_KEY);
        session.invalidate();
        this.login();
    }
    
    public void login() throws Exception
    {
    	this.context.put(Global.LOGIN_KEY, null);
    	this.context.put("moduleName", "login");
    	this.context.put("action", "validateLogin");
    	this.setTemplateName("login.htm");
    }
    
    public void validateLogin() throws Exception
    {
    	String username = request.getParameter("username");
    	String password = request.getParameter("password");
        User user = userDAO.get(username, password);
        HttpSession sess = request.getSession();
        sess.setAttribute(Global.LOGIN_KEY, user);
        if(user == null)
        {
        	this.context.put(Global.LOGIN_KEY, null);
        	this.context.put("moduleName", "login");
        	this.context.put("action", "validateLogin");
        	this.setTemplateName("login.htm");
        } else
        {
            this.context.put("user", user);
        	this.context.put("moduleName", "user");
        	this.context.put("action", "save");
        	this.context.put("moduleAction", "user.htm");
        	this.setTemplateName("admin/_layout.htm");
        }
    }
    
	public void list() throws Exception {
		// TODO Auto-generated method stub
	}
}
