package cn.outshine.stats.action;

import cn.outshine.stats.bean.Paginator;
import cn.outshine.stats.dao.*;
import cn.outshine.stats.util.StringUtil;



public class GuestAction extends AdminCommand
{
	private GuestDAOHibernate guestDAO = new GuestDAOHibernate();
	private UserDAO userDAO = new UserDAOHibernate();
	
    public GuestAction()
    {
    }

    public void list() throws Exception
    {
        Integer p = StringUtil.parseInt(request.getParameter("p"), 1);
        Integer userid = StringUtil.parseInt(request.getParameter("userid"));
        String ip = request.getParameter("ip");
    	Paginator guests = guestDAO.getPaginatorByWebsiteId(userid==0?null:userid, ip, 100, p);
    	this.context.put("guests", guests);
        this.context.put("userid", userid);
        this.context.put("sitelist", userDAO.getList());
    	this.context.put("moduleName", "guest");
    	this.context.put("action", "list");
    	this.context.put("moduleAction", "guest.htm");
    }
}
