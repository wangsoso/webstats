package cn.outshine.stats.model;

import java.util.Date;

public class Guest extends BaseModel
{
	
    private Integer id;
    private String website;
    private String webpage;
    private String ip;
    private String area;	
    private Date time;
    private String alexa;
    private String keyword;
    private String referer;
    private User user;
	
    public Guest()
    {
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public Date getTime()
    {
        return time;
    }

    public void setTime(Date time)
    {
        this.time = time;
    }

    public String getWebsite()
    {
        return website;
    }

    public void setWebsite(String website)
    {
        this.website = website;
    }

    public String getWebpage()
    {
        return webpage;
    }

    public void setWebpage(String webpage)
    {
        this.webpage = webpage;
    }
    
    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }	

    public String getArea()
    {
        return area;
    }

    public void setArea(String area)
    {
        this.area = area;
    }	
    
    public String getAlexa()
    {
        return alexa;
    }

    public void setAlexa(String alexa)
    {
        this.alexa = alexa;
    }
    
    public String getKeyword()
    {
        return keyword;
    }

    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }
    
    public String getReferer()
    {
        return referer;
    }

    public void setReferer(String referer)
    {
        this.referer = referer;
    }
}
