package cn.outshine.stats.model;

import java.io.Serializable;
import org.apache.commons.lang.builder.*;

public class BaseModel implements Serializable
{

    public BaseModel()
    {
    }

    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    public boolean equals(Object o)
    {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
