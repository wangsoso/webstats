package cn.outshine.stats.bean;

import cn.outshine.stats.model.BaseModel;

import java.util.List;

public class Paginator extends BaseModel
{

    public Paginator()
    {
    }

    public int getC()
    {
        return c;
    }

    public void setC(int c)
    {
        this.c = c;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public List getList()
    {
        return list;
    }

    public void setList(List list)
    {
        this.list = list;
    }

    public int getP()
    {
        return p;
    }

    public void setP(int p)
    {
        this.p = p;
    }

    public int getRange()
    {
        return range;
    }

    public void setRange(int range)
    {
        this.range = range;
    }

    private int p;
    private int c;
    private int range;
    private int count;
    private List list;
}
