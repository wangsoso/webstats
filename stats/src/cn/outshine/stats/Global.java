package cn.outshine.stats;

public class Global
{
	
    public static String basePath;
    public static String LOGIN_KEY = "COMSHINE_USER";
    public static String ENCODING = "UTF-8";
    
    public static final String count = "cn.outshine.stats.action.CountAction";
    public static final String login = "cn.outshine.stats.action.LoginAction";
    public static final String guest = "cn.outshine.stats.action.GuestAction";
    public static final String user = "cn.outshine.stats.action.UserAction";
    
    public Global(String basePath)
    {
    	this.basePath = basePath;
    }
    
    public static String getModuleClass(String model){
    	
    	if(model.equals("count")) return count;
    	if(model.equals("login")) return login;
    	if(model.equals("guest")) return guest;
    	if(model.equals("user")) return user;
    	else return null;
    }


}
