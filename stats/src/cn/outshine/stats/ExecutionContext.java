package cn.outshine.stats;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.SimpleHash;

public class ExecutionContext
{
    private static ThreadLocal userData = new ThreadLocal();
	private static Configuration templateConfig;
	private String redirectTo;
    private RequestContext request;
    private HttpServletResponse response;
    private SimpleHash context = new SimpleHash(ObjectWrapper.BEANS_WRAPPER);
    private String contentType;
    private boolean isCustomContent;
	
	/**
	 * Gets the execution context.
	 * @return
	 */
	public static ExecutionContext get()
	{
		ExecutionContext ex = (ExecutionContext)userData.get();

		if (ex == null) {
			ex = new ExecutionContext();
			userData.set(ex);
		}
		return ex;
	}
	
	/**
	 * Checks if there is an execution context already set
	 * @return <code>true</code> if there is an execution context
	 * @see #get()
	 */
	public static boolean exists()
	{
		return (userData.get() != null);
	}
	
	/**
	 * Sets the default template configuration 
	 * @param config The template configuration to set
	 */
	public static void setTemplateConfig(Configuration config)
	{
		templateConfig = config;
	}
	
	/**
	 * Gets a reference to the default template configuration settings.
	 * @return The template configuration instance
	 */
	public static Configuration templateConfig()
	{
		return templateConfig;
	}
	
	/**
	 * Sets the execution context
	 * @param ex
	 */
	public static void set(ExecutionContext ex)
	{
		userData.set(ex);
	}
	
	/**
	 * Sets a request to the execution context.
	 * @param request The request to set
	 */
	public void setRequest(RequestContext request)
	{
		this.request = request;
	}

	/**
	 * Gets the current thread's request
	 * @return
	 */
	public static RequestContext getRequest() {
		return ((ExecutionContext)userData.get()).request;
	}
	
	/**
	 * Sets the reponse to the execution context.
	 * @param response The response to set
	 */
	public void setResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	/**
	 * Gets the current thread's response
	 * @return
	 */
	public static HttpServletResponse getResponse() {
		return ((ExecutionContext)userData.get()).response;
	}

	/**
	 * Gets the current thread's template context
	 * @return
	 */
	public static SimpleHash getTemplateContext() {
		return ((ExecutionContext)userData.get()).context;
	}
	
	public static void setRedirect(String redirect) {
		((ExecutionContext)userData.get()).redirectTo = redirect;
	}
	/**
	 * Sets the content type for the current http response.
	 * @param contentType
	 */
	public static void setContentType(String contentType) {
		((ExecutionContext)userData.get()).contentType = contentType;
	}
	
	/**
	 * Gets the content type for the current request.
	 * @return
	 */
	public static String getContentType()
	{
		return ((ExecutionContext)userData.get()).contentType;
	}
	
	public static String getRedirectTo()
	{
		ExecutionContext ex = (ExecutionContext)userData.get();
		return (ex != null ? ex.redirectTo : null);
	}
	/**
	 * Marks the request to use a binary content-type.
	 * @param enable 
	 */
	public static void enableCustomContent(boolean enable) {
		((ExecutionContext)userData.get()).isCustomContent = enable;
	}
	
	/**
	 * Checks if the current request is binary
	 * @return <code>true</code> if the content tyee for the current request is 
	 * any binary data.
	 */
	public static boolean isCustomContent()
	{
		return ((ExecutionContext)userData.get()).isCustomContent;
	}
	
	public static void finish()
	{
		userData.set(null);
	}
    /**
     * Send UNAUTHORIZED to the browser and ask user to login via basic authentication
     *
     * @throws IOException
     */
    public static void requestBasicAuthentication() throws IOException {
        getResponse().addHeader("WWW-Authenticate", "Basic realm=\"vina\"");
        getResponse().sendError(HttpServletResponse.SC_UNAUTHORIZED);
        enableCustomContent(true);
    }
    
}
