package cn.outshine.stats;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class RequestContext extends HttpServletRequestWrapper 
{

	private Map query;

	public RequestContext(HttpServletRequest superRequest) throws IOException
	{
		super(superRequest);

		this.query = new HashMap();
		this.addParameter("module", "login");
		this.addParameter("action", "login");

		String encoding = Global.ENCODING;
		String requestType = (superRequest.getMethod()).toUpperCase();
		boolean isPost = "POST".equals(requestType);
		superRequest.setCharacterEncoding(encoding);
		String containerEncoding = "ISO-8859-1";
		
		if (isPost) { 
			containerEncoding = encoding;
		}
		
		for (Enumeration e = superRequest.getParameterNames(); e.hasMoreElements(); ) {
			String name = (String)e.nextElement();
			this.query.put(name, new String(superRequest.getParameter(name).getBytes(containerEncoding), encoding));
		}
	}

	public String getParameter(String parameter) 
	{
		return (String)this.query.get(parameter);
	}

	public Object getObjectParameter(String parameter)
	{
		return this.query.get(parameter);
	}

	public void addParameter(String name, Object value)
	{
		this.query.put(name, value);
	}

	public String getAction()
	{
		String action = this.getParameter("action");
		return action;		
	}
	
	public Object getObjectRequestParameter(String parameter)
	{
		return this.query.get(parameter);
	}
}