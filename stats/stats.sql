/*
Navicat MySQL Data Transfer

Source Server         : tencent
Source Server Version : 50559
Source Host           : localhost:3306
Source Database       : stats

Target Server Type    : MYSQL
Target Server Version : 50559
File Encoding         : 65001

Date: 2018-09-27 10:02:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for stats_guest
-- ----------------------------
DROP TABLE IF EXISTS `stats_guest`;
CREATE TABLE `stats_guest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `webpage` varchar(100) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `area` varchar(100) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `keyword` varchar(150) DEFAULT NULL,
  `alexa` varchar(100) DEFAULT NULL,
  `referer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2856 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for stats_user
-- ----------------------------
DROP TABLE IF EXISTS `stats_user`;
CREATE TABLE `stats_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
